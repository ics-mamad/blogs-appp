Store Configuration (store/Store.jsx):
Redux store is configured using the configureStore function.
The root reducer is provided to the store, where the root reducer is a combination of the blogReducer (from store/slices/BlogSlice.jsx).
The store is exported for use in the application.

// store/Store.jsx
import { configureStore } from "@reduxjs/toolkit";
import blogReducer from "./slices/BlogSlice";

const store = configureStore({
  reducer: {
    blogs: blogReducer,
  },
});

export { store };



Redux Slice (store/slices/BlogSlice.jsx):
The Redux slice for managing blogs is created using createSlice.
The initial state is set to an empty array.
Reducers (addBlog, removeBlog, editBlog) are defined to handle actions and update the state.

// store/slices/BlogSlice.jsx
import { createSlice } from "@reduxjs/toolkit";

const blogSlice = createSlice({
  name: "blogs",
  initialState: [],
  reducers: {
    addBlog: (state, action) => {
      return [...state, action.payload];
    },
    removeBlog: (state, action) => {
      return state.filter((blog) => blog.id !== action.payload);
    },
    editBlog: (state, action) => {
      return state.map((blog) =>
        blog.id === action.payload.id ? { ...blog, title: action.payload.title } : blog
      );
    },
  },
});

export const { addBlog, removeBlog, editBlog } = blogSlice.actions;
export default blogSlice.reducer;
Application Components (AddBlogs.jsx, Blogs.jsx, Home.jsx):
AddBlogs Component:
Provides a form to add a new blog.
When the user clicks submit, it creates a new blog object, stores it in local storage, and dispatches an action to add the blog to the Redux store.



// components/AddBlogs.jsx
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { addBlog } from "../store/slices/BlogSlice";

const AddBlogs = () => {
  // ... (omitted for brevity)

  const handleSubmit = () => {
    const newItem = { id: Math.random(), title: title };

    localStorage.setItem(newItem.id, JSON.stringify(newItem));

    dispatch(addBlog(newItem));

    setTitle("");
  };
  
  // ... (omitted for brevity)
};
Blogs Component:
Fetches blogs from both Redux store and local storage.
Checks for new blogs in local storage not in Redux store and dispatches addBlog action.
Displays a list of blogs with options to edit or delete.



// components/Blogs.jsx
import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { addBlog, removeBlog, editBlog } from "../store/slices/BlogSlice";
import { FaEdit, FaTrash } from "react-icons/fa";

const Blogs = () => {
  // ... (omitted for brevity)

  const editTitle = (id) => {
    // ... (omitted for brevity)
  };

  const saveEdit = () => {
    // ... (omitted for brevity)
  };

  const cancelEdit = () => {
    // ... (omitted for brevity)
  };

  const removeTitle = (id) => {
    // ... (omitted for brevity)
  };

  const updateLocalStorage = (data) => {
    // ... (omitted for brevity)
  };

  return (
    <div>
      <h1>List Of Blogs</h1>
      <ul>
        {blogs.map((blog) => (
          <li key={blog.id}>
            {editID === blog.id ? (
              <>
                {/* ... (omitted for brevity) */}
              </>
            ) : (
              <>
                {/* ... (omitted for brevity) */}
              </>
            )}
            <br />
            <br />
          </li>
        ))}
      </ul>
    </div>
  );
};
Home Component:
Displays the count of blogs using the useSelector hook to access the blogs array from the Redux store.



// components/Home.jsx
import React from "react";
import { useSelector } from "react-redux";

const Home = () => {
  const blogs = useSelector((state) => state.blogs);

  return (
    <div>
      <h1>Count Of Blogs : {blogs.length}</h1>
    </div>
  );
};
Application Entry Point (App.js):
Sets up the routing using react-router-dom.
Defines routes for Home, Blogs, and AddBlog components.



// App.js
import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Home from './components/Home';
import Blogs from './components/Blogs';
import AddBlog from './components/AddBlog';
import Navbar from './components/NavBar';

function App() {
  return (
    <BrowserRouter>
      <Navbar />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/blogs" element={<Blogs />} />
        <Route path="/addblog" element={<AddBlog />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
Index File (index.js):
Renders the App component within the Provider from react-redux, providing the Redux store.



// index.js
import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { store } from './store/Store';
import { Provider } from 'react-redux';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Provider store={store}>
    <App />
  </Provider>,
);

reportWebVitals();
