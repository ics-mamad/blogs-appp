import './App.css';
import {BrowserRouter, Routes, Route} from 'react-router-dom'
import Home from './components/Home';
import Blogs from './components/Blogs';
import AddBlog from './components/AddBlog';
import Navbar from './components/NavBar';



function App() {
  return (
    <div style = {{height:"100vh"}} className='full-height gradient_background' >
    <BrowserRouter>
    <Navbar />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/blogs" element={<Blogs />} />
        <Route path="/addblog" element={<AddBlog />} />
      </Routes>
    </BrowserRouter>
    </div>
  );
}

export default App;
