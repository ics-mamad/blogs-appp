import React from "react";
import { useSelector } from "react-redux";

const Home = () => {
  // Used the useSelector hook to access the 'blogs' array from the Redux store
  const blogs = useSelector((state) => state.blogs);

  return (
    <div className="border w-fit border-secondary border-3  " style={{height:"792px"}}>
      <h1 style={{marginLeft:"39%"}}>Count Of Blogs : {blogs.length}</h1>
    </div>
  );
};

export default Home;
