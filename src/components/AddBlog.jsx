import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { addBlog } from "../store/slices/BlogSlice";

const AddBlogs = () => {
  const dispatch = useDispatch();

  const [title, setTitle] = useState("");

  const handleSubmit = () => {
    const newItem = { id: Math.random(), title: title };

    localStorage.setItem(newItem.id, JSON.stringify(newItem)); //stores in local storage

    dispatch(addBlog(newItem)); //dispatch to store in redux store

    setTitle(""); //clears a add blog field after submit
  };

  return (
    <div className="border w-fit border-secondary border-3 " style={{height:"792px"}}>
    <div className="d-flex justify-content-center align-items-center" >
      
      <form className="row g-3">
      <h1 style={{marginLeft:"28%"}}>Add Blog</h1>
        <textarea onChange={(e) => setTitle(e.target.value)} value={title} className="form-control" rows={3}></textarea>
        {/* <br />
        <br /> */}
        <button type="button" className="btn btn-success" onClick={handleSubmit}>
          submit
        </button>
        
      </form>
    </div>
    </div>
  );
};

export default AddBlogs;
