import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { addBlog, removeBlog, editBlog } from "../store/slices/BlogSlice";
import { FaEdit, FaTrash } from "react-icons/fa";

const Blogs = () => {
  const [editID, setEditID] = useState(null);
  const [editedTitle, setEditedTitle] = useState("");

  const blogs = useSelector((state) => state.blogs);
  const dispatch = useDispatch();

  useEffect(() => {
    const storedData =
      Object.keys(localStorage).map((key) =>
        JSON.parse(localStorage.getItem(key))
      ) || [];

    // Check if the blog is not already in Redux store before dispatching
    storedData.forEach((blog) => {
      if (!blogs.some((item) => item.id === blog.id)) {
        dispatch(addBlog(blog));
      }
    });
  }, [blogs, dispatch]);

  const editTitle = (id) => {
    setEditID(id);
    const blogToEdit = blogs.find((blog) => blog.id === id);
    setEditedTitle(blogToEdit.title);
  };

  const saveEdit = () => {
    dispatch(editBlog({ id: editID, title: editedTitle }));

    // Update local storage
    const updatedBlogs = blogs.map((blog) =>
      blog.id === editID ? { ...blog, title: editedTitle } : blog
    );
    updateLocalStorage(updatedBlogs);

    alert("Blog Updated Successfully !!!")

    setEditID(null);
    setEditedTitle("");
  };

  const cancelEdit = () => {
    setEditID(null);
    setEditedTitle("");
  };

  const removeTitle = (id) => {
    dispatch(removeBlog(id));

    // Update local storage
    const updatedBlogs = blogs.filter((blog) => blog.id !== id);
    updateLocalStorage(updatedBlogs);

    alert("Blog Deleted Successfully !!!")
  };

  const updateLocalStorage = (data) => {
    // Update local storage with the latest data
    localStorage.clear();
    data.forEach((blog) => {
      localStorage.setItem(blog.id, JSON.stringify(blog));
    });
  };

  return (
    <div className="border w-fit border-secondary border-3 " style={{height:"792px"}}>
      <h1 style={{marginLeft:"42%"}}>List Of Blogs</h1><br />
      <div className="d-flex align-items-center">
      <ol style={{marginLeft:"42%"}}>
        {blogs.map((blog) => (
          <li key={blog.id} >
            {editID === blog.id ? (
              <>
                <input
                  type="text" className=""
                  value={editedTitle}
                  onChange={(e) => setEditedTitle(e.target.value)}
                />{" "}
                <button type="button" onClick={saveEdit} className="btn-success">
                  Save
                </button>{" "}
                <button type="button" onClick={cancelEdit} className="btn-danger">
                  Cancel
                </button>
              </>
            ) : (
              <>
                {blog.title}
                <span style={{ marginLeft: "20px" }}>
                  <button
                    type="button"
                    onClick={() => editTitle(blog.id)}
                  >
                    <FaEdit />
                  </button>{" "}
                  <button
                    type="button"
                    onClick={() => removeTitle(blog.id)}
                  >
                    <FaTrash />
                  </button>
                </span>
              </>
            )}
            <br />
            <br />
          </li>
        ))}
      </ol>
      </div>
    </div>
  );
};

export default Blogs;
