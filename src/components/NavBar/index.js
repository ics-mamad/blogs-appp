import React from "react";
import {
	Nav,
	NavLink,
	Bars,
	NavMenu,
} from "./navbarElement";

const Navbar = () => {
	return (
		<div>
			<Nav style={{marginLeft:"20%"}}>
				<Bars />
				<NavMenu>
					<NavLink to="/" >
						Home
					</NavLink>
					<NavLink to="/blogs" activeStyle>
                        Blogs
					</NavLink>
					<NavLink to="/addblog" activeStyle>
						Add-Blog
					</NavLink>
				</NavMenu>
			</Nav>
		</div>
	);
};

export default Navbar;
