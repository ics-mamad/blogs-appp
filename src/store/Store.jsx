import { configureStore } from "@reduxjs/toolkit";
import blogReducer from "./slices/BlogSlice";

const store = configureStore({
  reducer: {
    blogs: blogReducer,
  },
});

export { store };
