import { createSlice } from "@reduxjs/toolkit";

const blogSlice = createSlice({
  name: "blogs",
  initialState: [],
  reducers: {
    addBlog: (state, action) => {
      return [...state, action.payload];
    },
    removeBlog: (state, action) => {
      return state.filter((blog) => blog.id !== action.payload);
    },
    editBlog: (state, action) => {
      return state.map((blog) =>
        blog.id === action.payload.id ? { ...blog, title: action.payload.title } : blog
      );
    },
  },
});

export const { addBlog, removeBlog, editBlog } = blogSlice.actions;
export default blogSlice.reducer;
